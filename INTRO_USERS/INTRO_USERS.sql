--CREATE DATABASE INTRO_USERS
--GO

--USE INTRO_USERS
--GO

CREATE TABLE Admin (
	UserName varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	Email varchar(50) NOT NULL
	)

CREATE TABLE ClientType (
	Name varchar(30) NOT NULL PRIMARY KEY,
	Cost varchar(10) NOT NULL,
	Hours int NOT NULL
	)

CREATE TABLE Document (
	ID int NOT NULL PRIMARY KEY,
	Date date NOT NULL,
	Link varchar(30) NOT NULL,
	Type varchar(30) NOT NULL
	)

CREATE TABLE Car (
	License varchar(20) NOT NULL PRIMARY KEY,
	Make varchar(20) NOT NULL
	)

CREATE TABLE Instructor (
	Username varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(30) NOT NULL,
	Email varchar(50) NOT NULL,
	Phone varchar(20) NOT NULL
	)

CREATE TABLE Appointment (
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Notes varchar(1000)
	)

CREATE TABLE Timeslot (
	Id int IDENTITY(1,1) NOT NULL,
	Appointment_Id int NOT NULL UNIQUE,
	Time varchar(20) NOT NULL,
	Date varchar(20) NOT NULL,
	PRIMARY KEY (Id, Time, Date),
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)
		
CREATE TABLE Client (
	Username varchar(20) NOT NULL PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	Password varchar(30) NOT NULL,
	Email varchar(50) NOT NULL,
	Phone varchar(20) NOT NULL,
	Type varchar(30) NOT NULL,
	FOREIGN KEY (Type) REFERENCES ClientType
	)

CREATE TABLE Books (
	Client_Username varchar(20) NOT NULL,
	Appointment_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Appointment_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)

CREATE TABLE Receives (
	Client_Username varchar(20) NOT NULL,
	Doc_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Doc_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Doc_Id) REFERENCES Document
	)

CREATE TABLE Assigned (
	License varchar(20) NOT NULL,
	Instructor_Username varchar(20) NOT NULL,
	Appointment_Id int NOT NULL,
	Confirmed varchar(3) NOT NULL,
	PRIMARY KEY (License, Instructor_Username, Appointment_Id),
	FOREIGN KEY (License) REFERENCES Car,
	FOREIGN KEY (Instructor_Username) REFERENCES Instructor,
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)

--Insert statements
INSERT INTO Car VALUES ('CX98R4', 'Nissan')
INSERT INTO Car VALUES ('PL1436', 'Nissan')
INSERT INTO Car VALUES ('CNM984', 'Toyota')
INSERT INTO Car VALUES ('THN235', 'Subaru')
INSERT INTO Car VALUES ('ML6731', 'Nissan')

INSERT INTO ClientType VALUES ('Beginner', '$200', 10)
INSERT INTO ClientType VALUES ('Intermediate','$150', 7)
INSERT INTO ClientType VALUES ('Advanced', '$100', 5)

INSERT INTO Admin VALUES ('Jimmy', 'James', 'Richards', 'password', 'jimmy@gmail.com')
INSERT INTO Admin VALUES ('Tiddles', 'Tyler', 'Dog', 'password', 'tiddles@gmail.com')
INSERT INTO Admin VALUES ('Benny', 'Ben', 'McDonald', 'password', 'benny@gmail.com')

INSERT INTO Instructor VALUES ('Sarah', 'Sarah', 'Parker', 'password', 'sarah@gmail.com', '0275239821')
INSERT INTO Instructor VALUES ('Jezza', 'Jeremy', 'Bruin', 'password', 'jezza@gmail.com', '0279851234')
INSERT INTO Instructor VALUES ('Lotte', 'Liam', 'Wilson', 'password', 'lotte@gmail.com', '0275738742')
INSERT INTO Instructor VALUES ('Jeffy', 'Sarah', 'Parker', 'password', 'jeffy@gmail.com', '0279873476')

INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('Parking')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('Advanced')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('Indicating')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('Keep speed under control')
INSERT INTO Appointment VALUES ('lift confidence')
INSERT INTO Appointment VALUES ('help with indicating')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('Cornering')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('New driver')
INSERT INTO Appointment VALUES ('')
INSERT INTO Appointment VALUES ('New driver')

INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (1, '10:30am', '2017/4/1')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (2, '9:00am', '2017/4/2')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (3, '9:30am', '2017/4/3')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (4, '11:30am', '2017/4/4')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (5, '10:30am', '2017/4/5')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (6, '8:30am', '2017/4/6')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (7, '11:30am', '2017/4/7')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (8, '8:30am', '2017/4/8')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (9, '8:30am', '2017/4/9')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (10, '8:30am', '2017/4/10')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (11, '11:30am', '2017/4/11')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (12, '8:30am', '2017/4/12')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (13, '10:30am', '2017/4/13')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (14, '8:30am', '2017/4/14')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (15, '8:30am', '2017/4/15')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (16, '11:30am', '2017/4/16')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (17, '9:00am', '2017/4/17')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (18, '9:30am', '2017/4/18')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (19, '10:30am', '2017/4/19')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (20, '8:30am', '2017/4/20')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (21, '8:30am', '2017/4/21')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (22, '10:30am', '2017/4/22')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (23, '8:30am', '2017/4/23')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (24, '11:30am', '2017/4/24')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (25, '8:30am', '2017/4/25')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (26, '11:30am', '2017/4/26')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (27, '8:30am', '2017/4/27')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (28, '10:30am', '2017/4/28')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (29, '8:30am', '2017/4/29')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (30, '8:30am', '2017/4/30')
INSERT INTO Timeslot (Appointment_Id, Time, Date) VALUES (31, '11:30am', '2017/4/31')

--Drop statements
/*DROP TABLE Admin
DROP TABLE ClientType
DROP TABLE Document
DROP TABLE Car
DROP TABLE Instructor
DROP TABLE Appointment
DROP TABLE Timeslot
DROP TABLE Client
DROP TABLE Books
DROP TABLE Receives
DROP TABLE Assigned*/

--Select Statements
/*Select * FROM Car
Select * FROM ClientType
Select * FROM Admin
Select * FROM Instructor
Select * FROM Appointment
Select * FROM Timeslot
Select * FROM Client*/